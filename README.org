#+title: ~corfu-popup~ - Corfu popup on terminal

Corfu uses child frames to display candidates.  This makes Corfu
unusable on terminal.  This package replaces that with popup/popon,
which works everywhere.  Use M-x corfu-popup-mode to enable.  You'll
probably want to enable it only on terminal.  In that case, put the
following in your init file:

#+begin_src emacs-lisp
(unless (display-graphic-p)
  (corfu-popup-mode +1))
#+end_src
